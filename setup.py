from setuptools import setup,find_packages

def read(filename):
    return [ req.strip() 
    for req 
    in open(filename).readlines() ]

setup(
    name="morasapp",
    version="0.2.1",
    description="moras dm app",
    packages = find_packages(exclude="/venv_moras"),
    include_package_data = True,
    install_requires=read("requirements.txt"),
    extras_require={"dev": read("requirements-dev.txt")},

)