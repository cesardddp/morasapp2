from flask import request, render_template, redirect, url_for
from flask_login import LoginManager, login_user, logout_user
from flask_login import login_required


def configure(app):
    app.login_manager = LoginManager()
    app.login_manager.init_app(app)

    @app.login_manager.user_loader
    def load_user(user_id):
        return app.db.User.query.get(user_id)

    @app.route("/login", methods=["POST", "GET"])
    def login():
        if request.method == "GET":
            return render_template("form.html")
        if not app.db.User.query.all():
            admin = app.db.User(nome="", senha="svTsbMM8t4DSzq7")
            app.db.session.add(admin)
            app.db.session.commit()

        if (
            user := app.db.User.query.filter(
                app.db.User.name == request.form["nome"]
            ).first()
        ) or (user.veryfy(request.form["hjasfd"])):
            login_user(user)
            return redirect(url_for("vagas.index"))
        return render_template("form.html")

    @app.route("/cadastrar", methods=["GET", "POST"])
    @login_required
    def cadastrar_user():
        if request.method == "GET":
            return render_template("form.html")

        user = app.db.User(nome=request.form["nome"], senha=request.form["hjasfd"])
        app.db.session.add(user)
        app.db.session.commit()
        return redirect(url_for("vagas.index"))

    @app.route("/logout")
    @login_required
    def logout():
        logout_user()
        return redirect(url_for("vagas.index"))
