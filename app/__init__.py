from flask import Flask
from .model import configure as config_db
from flask_migrate import Migrate
from .login import configure as configure_login


def create_app():
    app = Flask(__name__)
    app.secret_key = "teste"  # open("secret.key",'r').read()

    app.config["DEBUG"] = True
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///app.db"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = "False"

    config_db(app)

    Migrate(app, app.db)

    from .vagas import bp_vagas
    from .morador import bp_morador
    app.register_blueprint(bp_vagas)
    app.register_blueprint(bp_morador)

    configure_login(app)

    return app


app = create_app()
