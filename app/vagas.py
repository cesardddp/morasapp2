from flask import (
    Blueprint,
    current_app,
    render_template,
    redirect,
    request,
    url_for,
)
from flask_login import login_required
from .model import Vagas
# from .db_obj import set_morador, troca_camas


bp_vagas = Blueprint("vagas", __name__)


@bp_vagas.route("/")
def index():
    return render_template("home.html")


@bp_vagas.route("/vagas")
@login_required
def index_vagas():
    return """
            <div> <a href="/mapa">mapa</a> </div>
            <div> <a href="/mapa/set_morador">mapa set morador</a> </div>
            """


@bp_vagas.route("/mapa")  # ,methods=['GET'])
@login_required
def mapa():
    result = Vagas.query.all()
    res = {
        vaga.id: vaga.morador for vaga in result
    }  # formata a lista de vagas como um dicionario com o morador nelas

    return render_template("mapa.html", vagas=res)


@bp_vagas.route("/mapa/set_morador", methods=["POST"])
@login_required
def set_morador_em_vaga():
    # id_morador = request.form.get("cvvbn4jk")
    # vaga = request.form.get("vaga")
    # try:
    #     ...
    #     # set_morador(vaga, id_morador, current_app.db.session)
    # except Exception as e:
    #     return f"{e}"

    return redirect(url_for(".mapa"))


@bp_vagas.route("/chg_bed", methods=["GET", "POST"])
@login_required
def troca_cama():
    if request.method == "GET":
        return render_template("trocar_cama.html", error=1)
    else:
        try:
            # troca_camas
            (
                request.form.get("a_g_v!"),
                request.form.get("a_g_v@"),
                current_app.db.session,
            )
        except Exception as e:
            return render_template("trocar_cama.html", error=e)  # data={})
        else:
            return render_template("trocar_cama.html", error=None)


@bp_vagas.route("/quarto/<vaga>")
@login_required
def quarto_mostra(vaga):
    return ...  # f"{Quarto.mostra(vaga)}"


@bp_vagas.route("/import_simple")  # ,methods=['GET'])
def fodasse():
    return Vagas._cria_vagas_simple()
