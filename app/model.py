from flask_sqlalchemy import SQLAlchemy, BaseQuery
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash


class Queris(BaseQuery):
    def todas_vagas(self) -> list:
        return self.all()


db = SQLAlchemy(query_class=Queris)


def configure(app):
    db.init_app(app)
    db.Morador = Morador
    db.User = User
    db.Vagas = Vagas
    app.db = db


class Vagas(db.Model):
    __tablename__ = "vagas"
    id = db.Column(db.String(4), primary_key=True, autoincrement=False)

    morador = db.relationship("Morador", backref="vagas")

    def all(self):
        return self.query.all()

    def mostra_quarto(self, vaga: str):

        return self.query.get(vaga).morador

    def add_quarto(self, morador: db.Model):
        if len(self.morador) >= 3:
            print("quarto cheio")

        else:
            self.morador.append(morador)

    @classmethod
    def _cria_vagas_simple(cls):

        Vagas.query.delete()
        db.session.commit()
        for casa in range(1, 13):  # casa
            for quarto in range(1, 5):  # quarto

                if casa < 10:
                    id = "0" + str(casa) + str(quarto)
                else:
                    id = str(casa) + str(quarto)

                temp = Vagas(id=id)  # bloco=bloco,casa=casa_,cama=cama)
                db.session.add(temp)
        try:
            db.session.commit()
        except Exception as e:
            print("error simpe import", e)
        return str(Vagas.query.all()), 200

    def __repr__(self):
        return f"id: {self.id}, morador_vaga: {self.morador} "


class Morador(db.Model):
    __tablename__ = "morador"
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(15))
    # apelido = db.Column(db.String(12))
    # data_chegada = db.Column(db.DateTime)
    # data_saida = db.Column(db.DateTime)

    vaga_id = db.Column(db.String(4), db.ForeignKey("vagas.id"))

    def novo(nome, vaga=None):
        morador = Morador(nome=nome)
        try:
            db.session.add(morador)
            if vaga:
                teste = Vagas.query.get(vaga)
                teste.add_quarto(morador)
            db.session.commit()
        except Exception as e:
            return f"Erro em comitar novo_morador {e}"
        else:
            return morador

    def deletar_morador(morador_id: int) -> None:
        print(morador_id)
        db.session.delete(Morador.query.get(morador_id))
        db.session.commit()

    def __repr__(self):
        return f"{self.nome} {str(self.vaga_id)}"


class User(db.Model, UserMixin):
    __tablename__ = "user"
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    name = db.Column(db.String(40))
    pwd = db.Column(db.String(90))

    def __init__(self, nome, senha):
        # self.id = id
        self.name = nome
        self.pwd = generate_password_hash(senha)

    def veryfy(senha):
        return check_password_hash(self.pwd, senha)
