from flask import (
    Blueprint,
    current_app,
    request,
    render_template,
    redirect,
    url_for,
    escape,
    jsonify,
)
from .model import Morador, Vagas
from flask_login import login_required


bp_morador = Blueprint("morador", __name__)


@bp_morador.route("/morador/todos")
@login_required
def mostrar_todos_moradores():
    result = Morador.query.all()
    return render_template("morador_todos.html", result=result)


@bp_morador.route("/morador/deleta/", methods=("POST", "GET"))
@bp_morador.route("/morador/deleta/<status>", methods=("POST", "GET"))
@login_required
def deleta(status=1):
    if request.method == "GET":
        result = Morador.query.all()
        return render_template("morador_deletar.html", result=result, status=status)

    try:
        Morador.deletar_morador(request.form.get("cvvbn4jk"))
    except Exception as e:
        return redirect(url_for(".deleta", status=e))
    return redirect(url_for(".deleta", status="Sucess"))


@bp_morador.route("/morador/alterar/", methods=("POST", "GET"))
@login_required
def alterar():
    if request.method == "GET":
        return "acess error fidamãe "

    morador = Morador.query.filter(Morador.nome == request.form.get("nome")).first()
    novo_nome = request.form.get("novonome", None)
    if morador and novo_nome:
        morador.nome = novo_nome
        current_app.db.session.commit()
        return render_template(
            "ficha_morador.html",
            query=Morador.query.filter(Morador.nome == str(novo_nome)).all(),
        )
    return "novo nome não pode ser vazio"


@bp_morador.route("/moradores/", methods=("GET", "POST"))
@login_required
def procura():
    morador_nome = request.form.get("nome")
    query = Morador.query.filter(Morador.nome == str(morador_nome)).all()
    # if query is None:
    # return 'hi'
    return render_template("ficha_morador.html", query=query)


@bp_morador.route("/morador/inf/", methods=("GET", "POST"))
@login_required
def procura2():
    morador_nome = request.form.get("1", 26)
    query = Morador.query.get(morador_nome)
    print(morador_nome)
    # if query is None:
    # return 'hi'
    return (
        jsonify(nome=query.nome, casa=query.vaga_id[:2], quarto=query.vaga_id[:2]),
        200,
    )


@bp_morador.route("/morador/novo", methods=["GET", "POST"])
@login_required
def novo():
    """se a requisição for post
        pega nome e vaga do formulario, cria nova instancia de Morador e pesquisa a vaga
        caso cosga guardar a morador
        PRECISA DAR COMMIT PRA RECEBER A ID DO USUARIO PARA ADICIONAR NA VAGA

    """
    if request.method == "GET":
        return render_template("novo_morador.html", request=True, status="")

    nome = request.form.get("nome", "")
    vaga = request.form.get("casa") + request.form.get("quarto")

    if nome:
        morador = Morador.novo(nome, vaga)
        return render_template(
            "novo_morador.html", query=morador, request=False, status=""
        )
    else:
        return render_template(
            "novo_morador.html", request=True, status="nome não pode ser vazio"
        )


# testes
try:
    from .load_test import exporta, importa

    @bp_morador.route("/ks20203/")  # ,methods=['GET'])
    @login_required
    def ks20203():
        return importa()

    @bp_morador.route("/ks20204/")  # ,methods=['GET'])
    @login_required
    def ks20204():
        return exporta()


except:
    print("no load test")
